ARG BASE_IMAGE

FROM $BASE_IMAGE

ARG TARGETARCH

RUN apk add --no-cache \
  curl \
  gcompat \
  git \
  idn2-utils \
  jq \
  openssh \
  ansible \
  py3-jmespath \
  py3-netaddr 

WORKDIR /root

# # Override ENTRYPOINT since hashicorp/terraform uses `terraform`
# ENTRYPOINT []
